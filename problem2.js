// 2. Create a function createBoard which takes the boardName as argument and returns a promise which resolves with newly created board data.

const { APIKey, APIToken } = require("./secretData.js");

function createBoard(boardName) {
  return fetch(
    `https://api.trello.com/1/boards/?name=${boardName}&key=${APIKey}&token=${APIToken}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        return new Error("Response in NOT ok.");
      }
    })
    .catch((error) => {
      return error;
    });
}

module.exports = createBoard;
