// 4. Create a function getCards which takes a listId as argument and returns a promise which resolves with cards data

const { APIKey, APIToken } = require("./secretData.js");

function getCards(listId) {
  return fetch(
    `https://api.trello.com/1/lists/${listId}/cards?key=${APIKey}&token=${APIToken}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    }
  )
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        return new Error("Response in NOT ok.");
      }
    })
    .catch((error) => {
      return error;
    });
}

module.exports = getCards;
