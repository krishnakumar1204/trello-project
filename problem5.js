// 5. Create a function getAllCards which takes a boardId as argument and which uses getCards function to fetch cards of all the lists. Do note that the cards should be fetched simultaneously from all the lists.

const { APIKey, APIToken } = require('./secretData.js');
const getCards = require('./problem4.js');

function getAllCards(boardId) {

    return fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${APIKey}&token=${APIToken}`)
        .then((response) => {

            return response.json();
        })
        .then((listData) => {

            const listIds = listData.map((list) => list.id);
            const getCardReqs = listIds.map((listId) => getCards(listId));

            return Promise.all(getCardReqs);
        })
        .catch((error) => {
            console.error(error);
        });
}

module.exports = getAllCards;