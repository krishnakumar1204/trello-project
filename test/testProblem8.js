const deleteLists = require('../problem8.js');

const boardId = 'aBCurJPO'; // boardId of Board created in Problem 6

deleteLists(boardId)
    .catch((error) => {
        console.error(error);
    });