const createBoardListCard = require('../problem6.js');

const boardName = "Databases";

createBoardListCard(boardName)
    .then((cardData) => {
        console.log(cardData);
    })
    .catch((error) => {
        console.error(error);
    });