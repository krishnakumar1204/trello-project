const getCards = require('../problem4.js');

const listId = '663080e56314f9c2cc877cf4'; // React

getCards(listId)
    .then((cardData) => {
        console.log(cardData);
    })
    .catch((error) => {
        console.error(error.message);
    });