const createBoard = require('../problem2.js');

const boardName = 'Git_GitHub';

createBoard(boardName)
    .then((boardData) => {
        console.log(boardData);
    })
    .catch((error) => {
        console.error(error.message);
    });