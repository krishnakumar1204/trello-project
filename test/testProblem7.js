const deleteLists = require('../problem7.js');

const boardId = 'PA9ED12Y'; // boardId of Board created in Problem 6

deleteLists(boardId)
    .then(() => {
        console.log("All Lists deleted Successfully!")
    })
    .catch((error) => {
        console.error(error);
    });