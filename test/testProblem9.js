const updateCheckitems = require('../problem9.js');

const checklistId = '66325aeaae1cf890e07ca07e'; // checklist in 'Router' Card of 'React' List of 'Libraries' Board

updateCheckitems(checklistId)
    .then((checkItemData) => {
        console.log(checkItemData);
    })
    .catch((error) => {
        console.error(error);
    });