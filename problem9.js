// 9. Update all checkitems in a checklist to completed status simultaneously

const { APIKey, APIToken } = require('./secretData.js');

function updateCheckitems(checklistId) {


    function getCardId(checklistId) {

        return fetch(`https://api.trello.com/1/checklists/${checklistId}/cards?key=${APIKey}&token=${APIToken}`, {
            method: 'GET'
        })
            .then(res => res.json())
            .then((cardData) => {
                return cardData[0].id;
            })
            .catch(console.error)
    }


    return fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${APIKey}&token=${APIToken}`, {
        method: 'GET'
    })
        .then(res => res.json())
        .then((checkItemsData) => {

            const checkItemsId = checkItemsData.map((checkItem) => checkItem.id);

            return getCardId(checklistId)
                .then((cardId) => {

                    const updateCheckitemReqs = checkItemsId.map((checkItemId) => {

                        return fetch(`https://api.trello.com/1/cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}?key=${APIKey}&token=${APIToken}&state=complete`, {
                            method: 'PUT',
                            headers: {
                                'Accept': 'application/json'
                            }
                        })
                    })

                    return Promise.all(updateCheckitemReqs);
                })
        })
        .then((resArray) => {
            return Promise.all(resArray.map((res) => res.json()));
        })
        .catch(console.error);




}

module.exports = updateCheckitems;