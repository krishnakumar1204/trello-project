// 8. Delete all the lists created in Step 6 sequentially i.e. List 1 should be deleted -> then List 2 should be deleted etc.

const { APIKey, APIToken } = require('./secretData.js');

function deleteLists(boardId) {

    return fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${APIKey}&token=${APIToken}`, {
        method: 'GET'
    })
        .then(res => res.json())
        .then((listData) => {

            const listIds = listData.map((list) => list.id);

            function delList(listIds, index) {

                if (index < listIds.length) {

                    return fetch(`https://api.trello.com/1/lists/${listIds[index]}?key=${APIKey}&token=${APIToken}&closed=true`, {
                        method: 'PUT'
                    })
                        .then(() => {
                            console.log(`list${index + 1} deleted successfully!`);

                            delList(listIds, index + 1);
                        })
                        .catch(console.error);
                }
            }

            let index = 0;
            delList(listIds, index);
        })
        .catch((error) => {
            console.error(error);
        });
}

module.exports = deleteLists;