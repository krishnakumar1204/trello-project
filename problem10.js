// 10. Update all checkitems in a checklist to incomplete status sequentially i.e. Item 1 should be updated -> then wait for 1 second -> then Item 2 should be updated etc.

const { APIKey, APIToken } = require('./secretData.js');

function updateCheckitems(checklistId) {

    function getCardId(checklistId) {

        return fetch(`https://api.trello.com/1/checklists/${checklistId}/cards?key=${APIKey}&token=${APIToken}`, {
            method: 'GET'
        })
            .then(res => res.json())
            .then((cardData) => {
                return cardData[0].id;
            })
            .catch(console.error)
    }


    return fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${APIKey}&token=${APIToken}`, {
        method: 'GET'
    })
        .then(res => res.json())
        .then((checkItemsData) => {

            const checkItemsId = checkItemsData.map((checkItem) => checkItem.id);

            getCardId(checklistId)
                .then((cardId) => {

                    function update(checkItemsId, index) {
                        if (index < checkItemsId.length) {
                            setTimeout(() => {
                                fetch(`https://api.trello.com/1/cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemsId[index]}?key=${APIKey}&token=${APIToken}&state=incomplete`, {
                                    method: 'PUT',
                                    headers: {
                                        'Accept': 'application/json'
                                    }
                                })
                                    .then((res) => res.json())
                                    .then((checkItemData) => {

                                        console.log(checkItemData);

                                        update(checkItemsId, index + 1);
                                    })
                                    .catch(console.error);
                            }, 1000);
                        }
                    }

                    let index = 0;
                    update(checkItemsId, index);

                })
                .catch(console.error)
        })
        .catch(console.error)
}

module.exports = updateCheckitems;