// 7. Delete all the lists created in Step 6 simultaneously

const { APIKey, APIToken } = require('./secretData.js');

function deleteLists(boardId) {
    return fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${APIKey}&token=${APIToken}`, {
        method: 'GET'
    })
        .then(res => res.json())
        .then((listData) => {

            const listIds = listData.map((list) => list.id);

            const listDeleteReqs = listIds.map((listId) => {
                return fetch(`https://api.trello.com/1/lists/${listId}?key=${APIKey}&token=${APIToken}&closed=true`, {
                    method: 'PUT'
                })
            })

            Promise.all(listDeleteReqs);
        })
        .catch((error) => {
            console.error(error);
        });
}

module.exports = deleteLists;