// 6. Create a new board, create 3 lists simultaneously, and a card in each list simultaneously

const { APIKey, APIToken } = require('./secretData.js');

function createBoardListCard(boardName) {

    return fetch(`https://api.trello.com/1/boards/?name=${boardName}&key=${APIKey}&token=${APIToken}`, {
        method: 'POST'
    })
        .then((res) => res.json())
        .then((boardData) => {

            const boardId = boardData.id;
            const listNames = ['MongoDB', 'Oracle', 'MySQL'];

            return Promise.all(listNames.map((listName) => {
                return fetch(`https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${APIKey}&token=${APIToken}`, {
                    method: 'POST'
                });
            }))

        })
        .then((resArray) => {

            return Promise.all(resArray.map((res) => res.json()))
        })
        .then((listData) => {

            let listIds = listData.slice(0, 3).map((list) => list.id);

            return Promise.all(listIds.map((listId) => {

                return fetch(`https://api.trello.com/1/cards?idList=${listId}&key=${APIKey}&token=${APIToken}&name=Introduction`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json'
                    }
                });

            }));

        })
        .then((resArray) => {
            return Promise.all(resArray.map((res) => res.json()));
        })
        .catch(console.error);
}



module.exports = createBoardListCard;


